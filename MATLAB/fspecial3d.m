function h = fspecial3d(varargin)

[type, p2, p3] = ParseInputs(varargin{:});

switch type
    
    case 'gaussian' % Gaussian filter

     siz   = (p2-1)/2;
     std   = p3;
     
     [x,y,z] = meshgrid(-siz(3):siz(3),-siz(2):siz(2),-siz(1):siz(1));
     arg   = -(x.*x + y.*y + z.*z)/(2*std*std);

     h     = exp(arg);
     h(h<eps*max(h(:))) = 0;

     sumh = sum(h(:));
     if sumh ~= 0,
       h  = h/sumh;
     end;
     
end

%%%
%%% ParseInputs
%%%
function [type, p2, p3] = ParseInputs(varargin)

% default values
type      = '';
p2        = [];
p3        = [];

% Check the number of input arguments.
iptchecknargin(1,3,nargin,mfilename);

% Determine filter type from the user supplied string.
type = varargin{1};
type = iptcheckstrs(type,{'gaussian','sobel','prewitt','laplacian','log',...
                    'average','unsharp','disk','motion'},mfilename,'TYPE',1);
  
% default values
switch type
	case 'average'
      p2 = [3 3];  % siz
      
   case 'disk'
      p2 = 5;      % rad
      
   case 'gaussian'
      p2 = [3 3];  % siz
      p3 = 0.5;    % std
      
   case {'laplacian', 'unsharp'}
      p2 = 1/5;    % alpha
      
   case 'log'
      p2 = [5 5];  % siz
      p3 = 0.5;    % std
      
   case 'motion'
      p2 = 9;     % len
      p3 = 0;      % theta
   end
   

switch nargin
    case 1
        % FSPECIAL('average')
        % FSPECIAL('disk')
        % FSPECIAL('gaussian')
        % FSPECIAL('laplacian')
        % FSPECIAL('log')
        % FSPECIAL('motion')
        % FSPECIAL('prewitt')
        % FSPECIAL('sobel')
        % FSPECIAL('unsharp')
        % Nothing to do here; the default values have 
        % already been assigned.        
        
    case 2
       % FSPECIAL('average',N)
       % FSPECIAL('disk',RADIUS)
       % FSPECIAL('gaussian',N)
       % FSPECIAL('laplacian',ALPHA)
       % FSPECIAL('log',N)
       % FSPECIAL('motion',LEN)
       % FSPECIAL('unsharp',ALPHA)
       p2 = varargin{2};
 
       switch type
          case {'sobel','prewitt'}
              msg = sprintf('%s: Too many arguments for this type of filter.', upper(mfilename));
              eid = sprintf('Images:%s:tooManyArgsForThisFilter', mfilename);
              error(eid,msg);
          case {'laplacian','unsharp'}
              iptcheckinput(p2,{'double'},{'nonnegative','real',...
                                  'nonempty','finite','scalar'},...
                            mfilename,'ALPHA',2);
              if  p2 > 1
                  msg = sprintf('%s: ALPHA should be less than or equal 1 and greater than 0.', upper(mfilename));
                  eid = sprintf('Images:%s:outOfRangeAlpha', mfilename);
                  error(eid,msg);
              end
          case {'disk','motion'}
              iptcheckinput(p2,{'double'},...
                            {'positive','finite','real','nonempty','scalar'},...
                            mfilename,'RADIUS or LEN',2);
          case {'gaussian','log','average'}
              iptcheckinput(p2,{'double'},...
                            {'positive','finite','real','nonempty','integer'},...
                            mfilename,'HSIZE',2);
              if numel(p2) > 2
                  msg = 'HSIZE should have 1 or 2 elements.';
                  eid = sprintf('Images:%s:wrongSizeN', mfilename);
                  error(eid,msg);
              elseif numel(p2)==1
                  p2 = [p2 p2]; 
              end
       end       

       
    case 3
       % FSPECIAL('gaussian',N,SIGMA)
       % FSPECIAL('log',N,SIGMA)
       % FSPECIAL('motion',LEN,THETA)
       p2 = varargin{2};
       p3 = varargin{3};
       
       switch type
          case 'motion'
              iptcheckinput(p2,{'double'},...
                            {'positive','finite','real','nonempty','scalar'},...
                            mfilename,'LEN',2);
              iptcheckinput(p3,{'double'},...
                            {'real','nonempty','finite','scalar'},...
                            mfilename,'THETA',3);
          case {'gaussian','log'}
              iptcheckinput(p2,{'double'},...
                            {'positive','finite','real','nonempty','integer'},...
                            mfilename,'N',2);
              iptcheckinput(p3,{'double'},...
                            {'positive','finite','real','nonempty','scalar'},...
                            mfilename,'SIGMA',3);
              if numel(p2) > 3
                  msg = sprintf('%s: size(N) should be less than or equal 3.', upper(mfilename));
                  eid = sprintf('Images:%s:wrongSizeN', mfilename);
                  error(eid,msg);
              elseif numel(p2)==1
                  p2 = [p2 p2 p2]; 
              end
          otherwise   
              msg = sprintf('%s: Too many arguments for this type of filter.', upper(mfilename));
              eid = sprintf('Images:%s:tooManyArgsForThisFilter', mfilename);
              error(eid,msg);
      end
end