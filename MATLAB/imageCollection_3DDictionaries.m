%training (constructing dictionaries) with different upsampling factor in
%the three dimensions, so the low-res patch size is also anisotropic
%remarks:
%- dictionaries are trained using only high-resolution volumes
%- all volumes are assumed to be in .nii.gz format, so they are decompressed, opened and then compressed again
%- a binary mask volume of each training image is used to restrict the patch extraction area, this volume is also assumed to be in .nii.gz format, with the same filename but followed by '_mask' and the intensity values should be only 0 or 1

%parameters
%dimP: number of patches to extract per image
%dimI: number of images for constructing the dictionary (dimP*dimI is the final number of atoms in the dictionary)
%magnif: vector with the individual scaling factors per dimension (3-dim)
%plSize: vector with the low-res patch size (3-dim)
%dir: string with the complete route to image directory
%imgs: cell of strings with the filenames of the images to be processed (each filename as a string separated by a blank space) e.g. imgs = {'0003' '0005' '0007' '0009'};
%dictName: string with the filename for saving the dictionaries

function [Ah, Af] = imageCollection_3DDictionaries(dimP,dimI,magnif,plSize,dir,imgs,dictName)

%high-res patch size
phSize = plSize.*magnif;

%blurring kernel
h = fspecial3d('gaussian',[3 3 3],1);

%filter 3D matrices
%first-order Sobel kernel
mf1 = cat(3, [1 0 -1; 2 0 -2; 1 0 -1], [2 0 -2; 4 0 -4; 2 0 -2], [1 0 -1; 2 0 -2; 1 0 -1]);
%second-order Sobel kernel
mf2        = [1 2 0 -2 -1; 4 8 0 -8 -4; 6 12 0 -12 -6; 4 8 0 -8 -4; 1 2 0 -2 -1];
mf2(:,:,2) = [4 8 0 -8 -4; 16 32 0 -32 -16; 24 48 0 -48 -24; 16 32 0 -32 -16; 4 8 0 -8 -4];
mf2(:,:,3) = [6 12 0 -12 -6; 24 48 0 -48 -24; 36 72 0 -72 -36; 24 48 0 -48 -24; 6 12 0 -12 -6];
mf2(:,:,4) = [4 8 0 -8 -4; 16 32 0 -32 -16; 24 48 0 -48 -24; 16 32 0 -32 -16; 4 8 0 -8 -4];
mf2(:,:,5) = [1 2 0 -2 -1; 4 8 0 -8 -4; 6 12 0 -12 -6; 4 8 0 -8 -4; 1 2 0 -2 -1];
    
%low-res feature dictionary concatenation
Af = double(zeros(6*prod(phSize),dimP*dimI));
%high-res high-frequency information
Ah = double(zeros(prod(phSize),dimP*dimI));

dict = 1; 
cont = 1;
%while true
for i=1:dimI
   str = imgs{dict};
   
   %LOAD HIGH-RESOLUTION VOLUME
   gunzip([dir str '.nii.gz']);
   vol = load_untouch_nii([dir str '.nii']);
   gzip([dir str '.nii']);
   delete([dir str '.nii']);
   extImg = vol.img;
   Ihigh = double(extImg);

   %BUILD LOW-RESOLUTION VERSION BY BLURRING AND DOWNSAMPLING
   tdimg_blur = imfilter(Ihigh,h);
   Ilow_size = zeros(1,3);
   for l=1:3
	 if magnif(l)<1.0
		 Ilow_size(l)=size(tdimg_blur,l)+(1.0-magnif(l));
	 else
		 Ilow_size(l)=size(tdimg_blur,l);
	 end
   end
   [Fx Fy Fz] = meshgrid(1:size(tdimg_blur,2),1:size(tdimg_blur,1),1:size(tdimg_blur,3));
   [X Y Z] = meshgrid(1:magnif(2):Ilow_size(2),1:magnif(1):Ilow_size(1),1:magnif(3):Ilow_size(3));
   Ilow = ba_interp3(Fx,Fy,Fz,double(tdimg_blur),X,Y,Z,'cubic');
   
   %LOAD HIGH-RESOLUTION TISSUE MASK, BLUR AND DOWNSAMPLE, AND DILATE
   %TO DEFINE THE PATCH EXTRACTION REGION
   gunzip([dir str '_mask.nii.gz']);
   vol = load_untouch_nii([dir str '_mask.nii']);
   gzip([dir str '_mask.nii']);
   delete([dir str '_mask.nii']);
   extImg = vol.img;
   tdimg_blur = extImg;
   Imask = ba_interp3(Fx,Fy,Fz,double(tdimg_blur),X,Y,Z,'cubic');
   [x,y,z] = meshgrid(-2:2,-2:2,-2:2);
   se = (x/2).^2 + (y/2).^2 + (z/2).^2 <= 1;
   Imask = imdilate(Imask,se);
   
   clear vol extImg se x y z

   %VOLUME PRE-PROCESSING, TO EXTRACT FEATURES ONLY ONCE
   
   %for the low-resolution image
   %upsampled version of the low-resolution volume (bicubic interpolation)
   [Fx Fy Fz] = meshgrid(1:size(Ilow,2),1:size(Ilow,1),1:size(Ilow,3));
   [X Y Z] = meshgrid(1:1/magnif(2):size(Ilow,2)+(1-1/magnif(2)),1:1/magnif(1):size(Ilow,1)+(1-1/magnif(1)),1:1/magnif(3):size(Ilow,3)+(1-1/magnif(3)));
   bIlow = ba_interp3(Fx,Fy,Fz,double(Ilow),X,Y,Z,'cubic');
   
   clear Fx Fy Fz X Y Z
   
   %extracting features
   %x-y first- and second-order sobel
   f1 = imfilter(bIlow,mf1);
   f4 = imfilter(bIlow,mf2);
   %y-z first- and second-order sobel
   f2 = imfilter(bIlow,permute(mf1,[2 3 1]));
   f5 = imfilter(bIlow,permute(mf2,[2 3 1]));
   %x-z first- and second-order sobel
   f3 = imfilter(bIlow,permute(mf1,[3 1 2]));
   f6 = imfilter(bIlow,permute(mf2,[3 1 2]));
   

   %for the high-resolution image
   %store only the high-frequency information added
   bIhigh = double(Ihigh) - bIlow;
   
   disp('ready to start!')

   %BUILD THE DICTIONARIES

   l=1;
   while l<=dimP
	 %define random index in low-resolution image
	 R1l = unidrnd(size(Ilow,1)-(plSize(1)-1));
	 R2l = unidrnd(size(Ilow,2)-(plSize(2)-1));
	 R3l = unidrnd(size(Ilow,3)-(plSize(3)-1));
	 %extract the low-resolution random patch
	 I2l = double(Ilow(R1l:R1l+(plSize(1)-1),R2l:R2l+(plSize(2)-1),R3l:R3l+(plSize(3)-1)));
	 %extract the low-resolution mask patch
	 mk = Imask(R1l:R1l+(plSize(1)-1),R2l:R2l+(plSize(2)-1),R3l:R3l+(plSize(3)-1));

	 %extract the corresponding high-resolution random patch
	 R1h = round(1+magnif(1)*(R1l-1));
	 R2h = round(1+magnif(2)*(R2l-1));
	 R3h = round(1+magnif(3)*(R3l-1));
	 I2h = double(bIhigh(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));

	 if mean(I2l(:))>0.015 && single(mk(1+floor((plSize(1)-1)/2),1+floor((plSize(2)-1)/2),1+floor((plSize(3)-1)/2)))==1

		%for the low-resolution dictionary
		%extract the six feature vectors from the filtered volume
		pf1 = double(f1(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));
		pf2 = double(f2(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));
		pf3 = double(f3(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));
		pf4 = double(f4(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));
		pf5 = double(f5(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));
		pf6 = double(f6(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));
		%concatenate all six feature vectors in one
		Af(1:6*prod(phSize),cont) = [reshape(permute(pf1,[2 1 3]),prod(phSize),1); reshape(permute(pf2,[2 1 3]),prod(phSize),1); reshape(permute(pf3,[2 1 3]),prod(phSize),1); reshape(permute(pf4,[2 1 3]),prod(phSize),1); reshape(permute(pf5,[2 1 3]),prod(phSize),1); reshape(permute(pf6,[2 1 3]),prod(phSize),1)];
		
		%for the high-resolution dictionary
		%extract the high-frequency information
		pHF = double(bIhigh(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));
		Ah(1:prod(phSize),cont) = reshape(permute(pHF,[2 1 3]),prod(phSize),1);

		l = l+1;
		cont = cont+1;
	 end
   end

   dict = dict + 1;
end

%save the low-res (Af) and high-res (Ah) coupled dictionaries
%both in image (png, only used for verification) and matlab format
save([dir '/' dictName '_featureMatrixDict_' int2str(dimP*dimI) '_size_' int2str(plSize(1)) 'x' int2str(plSize(2)) 'x' int2str(plSize(3)) '.mat'],'Af');
imwrite(mat2gray(Af,[-255 255]),[dir '/' dictName '_featureMatrixDict_' int2str(dimP*dimI) '_size_' int2str(plSize(1)) 'x' int2str(plSize(2)) 'x' int2str(plSize(3)) '.png'],'png');
save([dir '/' dictName '_highFreqMatrixDict_' int2str(dimP*dimI) '_size_' int2str(plSize(1)) 'x' int2str(plSize(2)) 'x' int2str(plSize(3)) '.mat'],'Ah');
imwrite(mat2gray(Ah,[-255 255]),[dir '/' dictName '_highFreqMatrixDict_' int2str(dimP*dimI) '_size_' int2str(plSize(1)) 'x' int2str(plSize(2)) 'x' int2str(plSize(3)) '.png'],'png');

