%super-resolution of a low-res image, using a trained couple of low-res and high-res dictionaries
%a PCA is used for dimensionality reduction, reducing also the computational time
%remarks:
%- all low-res volumes are assumed to be in .nii.gz format, so they are decompressed, opened and then compressed again
%- a binary mask volume of each low-res image is used to restrict the reconstruction area (voxels outside the mask are only interpolated), 
%this volume is also assumed to be in .nii.gz format, with the same filename but followed by '_mask' and the intensity values should be only 0 or 1

%parameters
%numR: percentage of coefficients to select after PCA (good reconstructions are obtained with 1)
%plSize: vector with the low-res patch size (3-dim)
%magnif: vector with the individual scaling factors per dimension (3-dim)
%dirI: string with the complete route to image directory
%imgs: string with the filenames of the images to be processed (each filename separated by a blank space)
%dirD: string with the complete route to dictionary directory
%dictName: string with the filename of the dictionaries
%dimD: number of atoms in the dictionaries
function imageSuperResolution3D(numR,plSize,magnif,dirI,imgs,dirD,dictName,dimD)

remainI = imgs;

%general parameters
%high-res patch size
phSize = plSize.*magnif;
%percentage of reduction
numR = numR * 0.01;

%filter 3D matrices
% first-order Sobel kernel
mf1 = cat(3, [1 0 -1; 2 0 -2; 1 0 -1], [2 0 -2; 4 0 -4; 2 0 -2], [1 0 -1; 2 0 -2; 1 0 -1]);
% second-order Sobel kernel
mf2        = [1 2 0 -2 -1; 4 8 0 -8 -4; 6 12 0 -12 -6; 4 8 0 -8 -4; 1 2 0 -2 -1];
mf2(:,:,2) = [4 8 0 -8 -4; 16 32 0 -32 -16; 24 48 0 -48 -24; 16 32 0 -32 -16; 4 8 0 -8 -4];
mf2(:,:,3) = [6 12 0 -12 -6; 24 48 0 -48 -24; 36 72 0 -72 -36; 24 48 0 -48 -24; 6 12 0 -12 -6];
mf2(:,:,4) = [4 8 0 -8 -4; 16 32 0 -32 -16; 24 48 0 -48 -24; 16 32 0 -32 -16; 4 8 0 -8 -4];
mf2(:,:,5) = [1 2 0 -2 -1; 4 8 0 -8 -4; 6 12 0 -12 -6; 4 8 0 -8 -4; 1 2 0 -2 -1];


%for each subject in the list
while true
    
    [strI, remainI] = strtok(remainI, ' '); %#ok<STTOK>
    if isempty(strI), break; end

    %LOAD VOLUME
    %Revisar que este pillando bien los vols
    filename = strI{1};

    gunzip([dirI filename '.nii.gz']);
    vol = load_untouch_nii([dirI filename '.nii']);
    gzip([dirI filename '.nii']);
    delete([dirI filename '.nii']);
    Im_low = vol.img;

    %LOAD MASK
    
    gunzip([dirI filename '_mask.nii.gz']);
    vol_mk = load_untouch_nii([dirI filename '_mask.nii']);
    gzip([dirI filename '_mask.nii']);
    delete([dirI filename '_mask.nii']);
    Im_mask = vol_mk.img;
    clear vol_mk;

    %LOAD DICTIONARIES
    
    %load the reconstruction dictionaries
    load([dirD dictName '_featureMatrixDict_' int2str(dimD) '_size_' int2str(plSize(1)) 'x' int2str(plSize(2)) 'x' int2str(plSize(3)) '.mat']); %Af
    load([dirD  dictName '_highFreqMatrixDict_' int2str(dimD) '_size_' int2str(plSize(1)) 'x' int2str(plSize(2)) 'x' int2str(plSize(3)) '.mat']); %Ah
    
    %PRINCIPAL COMPONENT ANALYSIS, TO REDUCE DIMENSIONALITY OF FEATURE PATCHES
    [coef,score] = princomp(Af');
    sampleMean = mean(Af,2);
    Afr = score(:,1:floor(size(Af,1)*numR))';

    %VOLUME PRE-PROCESSING, TO EXTRACT FEATURES ONLY ONCE
    %upsampled version of the low-resolution volume (bicubic interpolation)
    disp('upsampling input image')
    [Fx Fy Fz] = meshgrid(1:size(Im_low,2),1:size(Im_low,1),1:size(Im_low,3));
    [X Y Z] = meshgrid(1:1/magnif(2):size(Im_low,2)+(1-1/magnif(2)),1:1/magnif(1):size(Im_low,1)+(1-1/magnif(1)),1:1/magnif(3):size(Im_low,3)+(1-1/magnif(3)));
    bIlow = ba_interp3(Fx,Fy,Fz,double(Im_low),X,Y,Z,'cubic');

    %extracting features
    disp('extracting features')
    %x-y first- and second-order Sobel
    f1 = imfilter(bIlow,mf1);
    f4 = imfilter(bIlow,mf2);
    %y-z first- and second-order Sobel
    f2 = imfilter(bIlow,permute(mf1,[2 3 1]));
    f5 = imfilter(bIlow,permute(mf2,[2 3 1]));
    %x-z first- and second-order Sobel
    f3 = imfilter(bIlow,permute(mf1,[3 1 2]));
    f6 = imfilter(bIlow,permute(mf2,[3 1 2]));
    
    
    %process the low-res image in raster-scan order
    %for each low-res patch, we solve the optimization problem to generate
    %a high-res patch
    
    sizeX = size(Im_low,1);
    sizeY = size(Im_low,2);
    sizeZ = size(Im_low,3);

    Inew = double(zeros(magnif(1)*sizeX,magnif(2)*sizeY,magnif(3)*sizeZ));
    disp('processing patch by patch')
    for i=1:plSize(1):sizeX-(plSize(1)-1)
       for j=1:plSize(2):sizeY-(plSize(2)-1)
          for k=1:plSize(3):sizeZ-(plSize(3)-1)
             %extract the low-res patch
             y2 = double(Im_low(i:i+(plSize(1)-1),j:j+(plSize(2)-1),k:k+(plSize(3)-1)));
             %extract the corresponding mask patch
             mk = double(Im_mask(i:i+(plSize(1)-1),j:j+(plSize(2)-1),k:k+(plSize(3)-1)));
             
             if mean(mk(:))==0

                 %fill with black patch
                 Xest2 = zeros(magnif(1)*size(y2,1),magnif(2)*size(y2,2),magnif(3)*size(y2,3));

             else

                 %extract the feature vectors for this patch
                 R1h = round(1+magnif(1)*(i-1));
                 R2h = round(1+magnif(2)*(j-1));
                 R3h = round(1+magnif(3)*(k-1));
                 pf1 = double(f1(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));
                 pf2 = double(f2(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));
                 pf3 = double(f3(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));
                 pf4 = double(f4(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));
                 pf5 = double(f5(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));
                 pf6 = double(f6(R1h:R1h+(phSize(1)-1),R2h:R2h+(phSize(2)-1),R3h:R3h+(phSize(3)-1)));
                 
                 %concatenate all four feature vectors in one
                 yf = [reshape(permute(pf1,[2 1 3]),prod(phSize),1); reshape(permute(pf2,[2 1 3]),prod(phSize),1); reshape(permute(pf3,[2 1 3]),prod(phSize),1); reshape(permute(pf4,[2 1 3]),prod(phSize),1); reshape(permute(pf5,[2 1 3]),prod(phSize),1); reshape(permute(pf6,[2 1 3]),prod(phSize),1)];
                 
                 %apply transformation to reduce dimensionality
                 proy = (yf - sampleMean)'*coef;
                 yfr = proy(1:floor(size(Af,1)*numR))';

                 %form the new dictionary
                 %using the low-res reduced feature dictionary
                 Dnew = [Afr];

                 %form the new observation (y)
                 %using the low-res reduced feature extraction
                 Ynew = [yfr];

                 %solve the sparse representation problem
                 x = SolveBP(Dnew,Ynew,dimD,20,0.01);

                 %generate the reconstructed high-frequency patch
                 Xest = Ah*x;
                 Xest2 = permute(reshape(Xest,phSize(2),phSize(1),phSize(3)),[2 1 3]);
                 
             end

             %put it in the reconstructed high-frequency image
             posX = round(1+magnif(1)*(i-1)):round(phSize(1)+magnif(1)*(i-1));
             posY = round(1+magnif(2)*(j-1)):round(phSize(2)+magnif(2)*(j-1));
             posZ = round(1+magnif(3)*(k-1)):round(phSize(3)+magnif(3)*(k-1));
             Inew(posX,posY,posZ) = Xest2;

          end
       end
    end
    
    %add the low-frequencies to complete the image
    Inew = Inew + bIlow;
    clear Af Afr f1 f2 f3 f4 f5 f6 coef score X Y Z Fx Fy Fx Im_mask Ah bIlow;

    disp('saving after local reconstruction')
    vol.hdr.dime.dim(2) = size(Im_low,1)*magnif(1);
    vol.hdr.dime.dim(3) = size(Im_low,2)*magnif(2);
    vol.hdr.dime.dim(4) = size(Im_low,3)*magnif(3);
    vol.hdr.dime.pixdim(2) = vol.hdr.dime.pixdim(2)/magnif(1);
    vol.hdr.dime.pixdim(3) = vol.hdr.dime.pixdim(3)/magnif(2);
    vol.hdr.dime.pixdim(4) = vol.hdr.dime.pixdim(4)/magnif(3);
    vol.img = int16(Inew);
    save_untouch_nii(vol,[dirI filename '_Step1_' int2str(dimD) '_pca' int2str(numR*100) '.nii']);
    gzip([dirI filename '_Step1_' int2str(dimD) '_pca' int2str(numR*100) '.nii']);
    delete([dirI filename '_Step1_' int2str(dimD) '_pca' int2str(numR*100) '.nii']);


    %finally, the global step
    %regularization of the resulting image using a back-projection algorithm

    %construct the Gaussian backprojection kernel
    p = fspecial3d('gaussian',[3 3 3],2.2);
    %construct the blurring operator
    h = fspecial3d('gaussian',[3 3 3],1);

    %start the iterative actualization
    Xnew = Inew;
    clear Inew;
    Xold = double(zeros(magnif(1)*sizeX,magnif(2)*sizeY,magnif(3)*sizeZ));

    Xdiff = abs(Xnew-Xold);

    while (mean(Xdiff(:))>0.3)
       Xold = Xnew;
       %blurring
       Xold_blur = imfilter(Xold,h,'replicate');
       %downsampling
       Ilow_size = zeros(1,3);
       for l=1:3
         if magnif(l)<1.0
             Ilow_size(l)=size(Xold_blur,l)+(1.0-magnif(l));
         else
             Ilow_size(l)=size(Xold_blur,l);
         end
       end
       [Fx Fy Fz] = meshgrid(1:size(Xold_blur,2),1:size(Xold_blur,1),1:size(Xold_blur,3));
       [X Y Z] = meshgrid(1:magnif(2):Ilow_size(2),1:magnif(1):Ilow_size(1),1:magnif(3):Ilow_size(3));
       Xold_sub = ba_interp3(Fx,Fy,Fz,Xold_blur,X,Y,Z,'cubic');
       %error in low-res approximation
       Ierror = double(Im_low) - Xold_sub;
       %upsampling
       [Fx Fy Fz] = meshgrid(1:size(Ierror,2),1:size(Ierror,1),1:size(Ierror,3));
       [X Y Z] = meshgrid(1:1/magnif(2):size(Ierror,2)+(1-1/magnif(2)),1:1/magnif(1):size(Ierror,1)+(1-1/magnif(1)),1:1/magnif(3):size(Ierror,3)+(1-1/magnif(3)));
       Ierror_up = ba_interp3(Fx,Fy,Fz,Ierror,X,Y,Z,'cubic');
       %convolution with backprojection kernel
       Iconv = imfilter(Ierror_up,p,'conv','replicate');
       %updating
       Xnew = Xold + Iconv;
       Xdiff = abs(Xnew-Xold);
    end

    disp('saving after global regularization')
    vol.img = int16(Xnew);

    save_untouch_nii(vol,[dirI filename '_Step2_' int2str(dimD) '_pca' int2str(numR*100) '.nii']);
    gzip([dirI filename '_Step2_' int2str(dimD) '_pca' int2str(numR*100) '.nii']);
    delete([dirI filename '_Step2_' int2str(dimD) '_pca' int2str(numR*100) '.nii']);

    disp(['finished with image ' filename])
end
