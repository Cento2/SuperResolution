function [vol_out]= downsampling(vol_in,magnif,blur_size,blur_sigma,fileprefix,rician)


h = fspecial3d('gaussian',blur_size,blur_sigma);

tdimg_blur = imfilter(vol_in.img,h);
Ilow_size = zeros(1,3);
for l=1:3
    if magnif(l)<1.0
        Ilow_size(l)=size(tdimg_blur,l)+(1.0-magnif(l));
    else
        Ilow_size(l)=size(tdimg_blur,l);
    end
end
[Fx Fy Fz] = meshgrid(1:size(tdimg_blur,2),1:size(tdimg_blur,1),1:size(tdimg_blur,3));
[X Y Z] = meshgrid(1:magnif(2):Ilow_size(2),1:magnif(1):Ilow_size(1),1:magnif(3):Ilow_size(3));
Ilow = ba_interp3(Fx,Fy,Fz,double(tdimg_blur),X,Y,Z,'cubic');

if nargin>5
    
    
    
    if rician ==1
        
        if nargin <6
            [ Ilow] = addRicianNoise( Ilow);
        else
            
            [ Ilow] = addRicianNoise( Ilow, noiseIntensity);
        end
    end
    
end
[vol_out]= create_nii(Ilow,fileprefix,vol_in.hdr);
end
