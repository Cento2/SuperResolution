function [vol]= create_nii(img,fileprefix,hdr)
    
%el hdr debe ser el de la imagen original y ahora modifico cosas
%para que cuadre con la nueva resolución


    filetype=2;
    machine='created in matlab';
    ext=[];
    untouch=1;
    hdr.dim=[3 size(img,1) size(img,2) size(img,3) 1 1 1 1];
    hdr.dime.glmax = round(double(max(img(:))));
    hdr.dime.glmin = round(double(min(img(:))));
    hdr.dime.pixdim = [1 (hdr.dime.dim(2:4).*hdr.dime.pixdim(2:4))./size(img) 0 0 0 0];
    hdr.dime.dim = [3 size(img) 1 1 1 1];
%     hdr.hist.srow_x=[ hdr.dime.pixdim(2) 0 0 0];
%     hdr.hist.srow_y=[0 hdr.dime.pixdim(3)  0 0];
%     hdr.hist.srow_z=[0 0 hdr.dime.pixdim(4) 0];
    vol=struct('hdr',hdr,'filetype',filetype,'fileprefix',fileprefix,...
       'machine' ,machine,'ext',ext,'img',img,'untouch',untouch);
    
end